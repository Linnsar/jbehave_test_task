package com.epam.mkoltsova.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Mariia_Koltsova on 10/13/2017.
 */
public class googleSearchPage {

    private WebDriver driver;
    private String searchUrl = "https://www.google.com.ua";

    @FindBy(id = "lst-ib")
    private WebElement searchField;

    @FindBy(name= "btnK")
    private WebElement buttonSearch;



    public googleSearchPage(WebDriver driver) {

        if (!driver.getCurrentUrl().contains(searchUrl)) {
            throw new IllegalStateException(
                    "This is not the page you are expected"
            );
        }

        PageFactory.initElements(driver, this);
        this.driver = driver;

    }

    private void searchText(String searchTerm) {

        searchField.sendKeys(searchTerm);
        buttonSearch.submit();

    }

    public calculatorPage searchSuccess() {
        searchText("calc");
        return new calculatorPage(driver);
    }




}
