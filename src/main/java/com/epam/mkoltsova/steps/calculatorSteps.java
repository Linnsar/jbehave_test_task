package com.epam.mkoltsova.steps;


import com.epam.mkoltsova.pages.calculatorPage;
import com.epam.mkoltsova.pages.googleSearchPage;
import net.serenitybdd.jbehave.SerenityJBehaveTestRunner;
import net.serenitybdd.junit.runners.SerenityRunner;
import org.hamcrest.Matchers;
import org.jbehave.core.annotations.*;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@RunWith(SerenityJBehaveTestRunner.class)

public class calculatorSteps {

    public static String driverName = "chromedriver.exe";
    String workingDirectory = System.getProperty("user.dir");
    private String url = "https://www.google.com.ua/";
    public static WebDriver driver;

    @BeforeStory
    public void setup() {

        System.setProperty("webdriver.chrome.driver", workingDirectory + System.getProperty("file.separator") + driverName);

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability("chrome.switches", Arrays.asList("--ignore-certificate-errors"));
        driver = new ChromeDriver(capabilities);
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

        driver.get(url);
        googleSearchPage sp = new googleSearchPage(driver);
        sp.searchSuccess();
    }
    @Given("I am on Google calculator Page")
    public void getCurrentURL() {
        calculatorPage calc = new calculatorPage(driver);

        Assert.assertEquals(driver.getCurrentUrl(), calc.calculatorUrl());
    }

    @When("I pass <operand1> <mathOperator> <operand2> and click equals button")
    @Alias("I pass $operand1 $mathOperator $operand2 and click equals button")
    public void mathOperation(@Named("operand1") int operand1, @Named("operand2") int operand2,
                              @Named("mathOperator") String mathOperator) {
        calculatorPage calc = new calculatorPage(driver);
        calc.executeOperation(operand1, operand2, mathOperator);

    }

    @Then("<result> should be displayed")
    @Alias("$result should be displayed")
    public void verifyResult(@Named("result") String amount) {
        calculatorPage calc = new calculatorPage(driver);

        Assert.assertThat(calc.inputValue(), Matchers.equalTo(amount));;
    }

}
