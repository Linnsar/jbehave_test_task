Narrative: I should be able to Navigate to Google Calculator page and execute basic arithmetical
operations, such as Addition, Subtraction, Multiplication, Division, Division by 0.

Scenario: I should see the result of Addition of two numbers

Given I am on Google calculator Page
When I pass <operand1> <mathOperator> <operand2> and click equals button
Then <result> should be displayed

Examples:
|operand1|mathOperator|operand2|result|
|22|+|8|30|


Scenario: I should see the result of Substraction of two numbers

Given I am on Google calculator Page
When I pass <operand1> <mathOperator> <operand2> and click equals button
Then <result> should be displayed

Examples:
|operand1|mathOperator|operand2|result|
|45|-|25|20|


Scenario: I should see the result of Multiplication of two numbers

Given I am on Google calculator Page
When I pass <operand1> <mathOperator> <operand2> and click equals button
Then <result> should be displayed

Examples:
|operand1|mathOperator|operand2|result|
|6|*|7|42|

Scenario: I should see the result of Division of two numbers

Given I am on Google calculator Page
When I pass <operand1> <mathOperator> <operand2> and click equals button
Then <result> should be displayed

Examples:
|operand1|mathOperator|operand2|result|
|60|/|5|12|

Scenario: I should see the result of Math Operation of dividing by zero

Given I am on Google calculator Page
When I pass <operand1> <mathOperator> <operand2> and click equals button
Then <result> should be displayed

Examples:
|operand1|mathOperator|operand2|result|
|68|/|0|Infinity|


